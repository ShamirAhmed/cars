let problem = function(inventory){
    if(inventory === undefined || Array.isArray(inventory) === false || inventory.length === 0){
        return [];
    }
    else{
        var prevProblem = require('./problem4');
        var result = prevProblem(inventory);
        let cars = [];
        for(let index=0;index<result.length;index++){
            if(result[index] < 2000)
                cars.push(inventory[index]);
        }
        return cars;
    }
}

module.exports = problem;