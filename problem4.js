var problem = function(inventory){
    if(inventory === undefined || Array.isArray(inventory) === false || inventory.length === 0){
        return [];
    }
    else{
        var year = [];
        for(let i=0;i<inventory.length;i++){
            year.push(inventory[i].car_year);
        }
        return year;
    }
}

module.exports = problem;