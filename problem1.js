let problem = function(inventory, searchId){
    if(inventory === undefined || searchId === undefined || Array.isArray(inventory) === false || inventory.length === 0 || typeof searchId !== 'number'){
        return [];
    }
    else{
        for(let index=0;index<inventory.length;index++){
            if(inventory[index].id == searchId)
                return inventory[index];
        }
        return [];
    }
};

module.exports = problem;