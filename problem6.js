var problem = function(inventory){
    if(inventory === undefined || Array.isArray(inventory) === false || inventory.length === 0){
        return [];
    }
    else{
        let cars = [];
        for(let i=0;i<inventory.length;i++){
            if(inventory[i].car_make === 'BMW' || inventory[i].car_make === 'Audi')
                cars.push(inventory[i]);
        }
        return cars;
    }
}

module.exports = problem;