let problem = function(inventory){
    if(inventory === undefined || Array.isArray(inventory) === false || inventory.length === 0){
        return [];
    }
    else
        return [`Last car is a ${inventory[inventory.length-1].car_make} ${inventory[inventory.length-1].car_model}`];
}

module.exports = problem;