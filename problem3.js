function compareCarModel(car1,car2){
    const model1 = car1.car_model.toUpperCase();
    const model2 = car2.car_model.toUpperCase();

    let compare = 0;

    if(model1 > model2)
        compare = 1;
    else if(model1 < model2)
        compare = -1;
    return compare;
}
let problem = function(inventory){
    if(inventory === undefined || Array.isArray(inventory) === false || inventory.length === 0){
        return [];
    }
    else
        return inventory.sort(compareCarModel);
}

module.exports = problem;